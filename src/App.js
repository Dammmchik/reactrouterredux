import logo from './logo.svg';
import './App.css';

import HomePage from './Components/HomePage';
import LoginPage from './Components/LoginPage';
import RegisterPage from './Components/RegisterPage';
import NotFoundPage from './Components/NotFoundPage';

import React from 'react';
import {
    BrowserRouter,
    Routes,
    Route
} from "react-router-dom"


function App() {
    return (
        <div>
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<HomePage />} />
                    <Route path="login" element={<LoginPage />} />
                    <Route path="register" element={<RegisterPage/>} />
                    <Route path="homepage" element={<HomePage />} />
                    <Route path="*" element={<NotFoundPage />} />
                </Routes>
            </BrowserRouter>
        </div>
    );

}

export default App;
