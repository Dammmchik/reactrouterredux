import './css/Component.css';
import React, { useState } from 'react';
import { Link } from "react-router-dom"
import { useDispatch, useSelector } from 'react-redux'
import AuthActions from '../Actions/authActions'

import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';
import { LinkContainer } from 'react-router-bootstrap'

export default function Header(props) {
    const dispatch = useDispatch();
    const stateObject = useSelector(state => state)
    const GetStatus = () =>
    {
        return 'Login status: ' + stateObject.status;
    }

    const GetToken = () => {
        return 'Token: ' + stateObject.token;
    }

    return (
        <div>
            <Navbar bg="light" expand="lg">
                <Container>
                    <Navbar.Brand href="">Homework</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="me-auto">
                            <LinkContainer to="/homepage">
                                <Nav.Link>Homepage</Nav.Link>
                            </LinkContainer>
                            <LinkContainer to="/login">
                                    <Nav.Link>Login</Nav.Link>
                            </LinkContainer>
                            <LinkContainer to="/register">
                                <Nav.Link>Register</Nav.Link>
                            </LinkContainer>
                            <Nav.Link onClick={() => dispatch(AuthActions.logOut())}>Logout</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>

            <h2 className="header">{ props.Title }</h2>

            <Container className="p-2">
            <div>
                <em>{GetStatus()}</em>
            </div>
            <div>
                <em>{GetToken()}</em>
                </div>
            </Container>
        </div>
    );
}