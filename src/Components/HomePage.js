import './css/Component.css';
import React, { useState } from 'react';
import Header from './Header';


export default function HomePage() {

    return (
        <div>
            <Header Title={'Home page'} />
        </div>
    );
}