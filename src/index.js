import React from 'react';
import { createRoot } from 'react-dom/client';
import './index.css';
import App from './App';
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import AuthReducer from './Reducers/authReducer'
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';

const reduxStore = createStore(AuthReducer)
const container = document.getElementById('root');
const root = createRoot(container);

root.render(
    <React.StrictMode>
        <Provider store={reduxStore}>
            <App />
        </Provider>
    </React.StrictMode>
);
